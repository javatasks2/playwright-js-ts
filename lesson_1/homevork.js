let num = 42;
let str = "Hello, World!";
let bool = true;
let nullValue = null;
let undefinedValue = undefined;
let sym = Symbol("foo");

let obj = { key: "value" };
let arr = [1, 2, 3];

function greet(name) {
  return "Hello, " + name + "!";
}

console.log(typeof num);           // "number"
console.log(typeof str);           // "string"
console.log(typeof bool);          // "boolean"
console.log(typeof nullValue);     // "object" (known quirk in JavaScript)
console.log(typeof undefinedValue);// "undefined"
console.log(typeof sym);           // "symbol"

console.log(typeof obj);           // "object"
console.log(typeof arr);           // "object" (arrays are objects in JavaScript)
console.log(typeof greet);         // "function"