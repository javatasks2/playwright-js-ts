/// 1)  STRING REVERSE
const string = "ABCDEF";
const stringRev = [...string].reverse().reduce((acc, currVal) => acc + currVal)
console.log(`STRING: ${string};\n REVERSED STRING: ${stringRev}`);

/// 2) POLINDROME
const poli = function (initStr) {
  const reversed = [...initStr].reverse().reduce((acc, currVal) => acc + currVal)
  console.log(reversed === initStr);
}

poli("ACA")
poli("BAOBOAB")
poli("AQA")
poli("QA")


/// 3) REVERSE ARRAY AND SHOW ONLY ODD VALUES
const reverseOdd = () => {
  const arr = [1, 2, 3, 4, 5, 6, 7]
  console.log(arr.reverse().filter(n => n % 2 == 0));
}

reverseOdd()

/// 4) FIND THE LONGES SUBSTR FOR STRING COLLECTION
function findLongesSubstr(arrOfStrs) {
  shortestStr = arrOfStrs[0]
  for (strEl of arrOfStrs) {
    console.log("STRING: " + strEl);
    strEl.length < shortestStr.length ? shortestStr = strEl : shortestStr
  }

  resultPrefix = "";
  buffPrefix = shortestStr[0]
  for (charEl of shortestStr) {
    counter = 0
    isPresent = true;
    while (isPresent) {
      if (counter < arrOfStrs.length && buffPrefix === arrOfStrs[counter].substr(0, buffPrefix.length)) {
        if (counter === arrOfStrs.length) {
          resultPrefix = buffPrefix
          isPresent = false
        } else {
          counter++
          continue
        }
      } else {
        buffPrefix = ""
        isPresent = false
      }
    }
  }
  console.log("LONGEST STR PREFIX: " + shortestStr);
}

findLongesSubstr(['abc', 'abcz', 'abcde', 'abcde'])