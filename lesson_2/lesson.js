// Function that takes a callback as a parameter
function fetchData(url, callback) {
  // Simulating an asynchronous operation (e.g., making an API request)
  setTimeout(function () {
    const data = { status: 200, message: "Data fetched successfully" };
    callback(data);
  }, 2000); // Simulating a delay of 2 seconds
}

// Callback function to handle the fetched data
function handleData(response) {
  console.log("Received data:", response);
}

// Calling the fetchData function with the handleData callback
fetchData("https://api.example.com/data", handleData);